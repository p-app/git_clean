# Git clean project.

Tool for removing old data from git repository.

## Installation.
```bash
sudo -H python3 -m pip3 install -U git-clean
```

## Usage.
- Removing remote old branches:
    ```bash
    clean-git --url=ssh://git@git.example.com/example.git
    ```
- Removing remote and local branches:
    ```bash
    clean-git --dirpath=/path/to/git/repo/root
    ```
- Removing remote and local branches, which last commit age is higher, than 360 days only.
    ```bash
    clean-git --dirpath=/path/to/git/repo/root --over-days=360
    ```
- Removing remote and local branches, which name is not "test".
    ```bash
    clean-git --dirpath=/path/to/git/repo/root --exclude-dirpath="^master|test$"
    ```
